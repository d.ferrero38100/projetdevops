#!/bin/bash

# Mise à jour et mise à niveau des paquets
sudo apt-get update -y
# Désactivation du swap et modification de /etc/fstab pour empêcher le swap de se remonter au démarrage
sudo swapoff -a && sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Configuration des modules pour containerd
sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF

# Chargement des modules
sudo modprobe overlay
sudo modprobe br_netfilter

# Configuration des paramètres du noyau pour Kubernetes
sudo tee /etc/sysctl.d/kubernetes.conf <<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

# Application des nouveaux paramètres du noyau
sudo sysctl --system

# Installation des dépendances pour Docker
sudo apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates

# Ajout de la clé GPG de Docker et ajout du dépôt Docker
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Installation de containerd
sudo apt update && sudo apt install -y containerd.io

# Configuration de containerd
containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
sudo systemctl restart containerd && sudo systemctl enable containerd

# Ajout de la clé GPG de Kubernetes et ajout du dépôt Kubernetes
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/kubernetes-xenial.gpg
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"

# Installation de Kubernetes
sudo apt update && sudo apt install -y kubelet kubeadm kubectl && sudo apt-mark hold kubelet kubeadm kubectl
